﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour {

    //Define Enum
    public enum TestEnum { Test1, Test2, Test3 };

    //This is what you need to show in the inspector.
    public TestEnum Tests;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
