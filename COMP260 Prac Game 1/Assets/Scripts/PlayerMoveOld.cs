﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveOld : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public Vector2 move;
	public Vector2 velocity; // in metres per second
	public float maxSpeed = 5.0f;

	void Update() {
		
   // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis("Horizontal1");
        direction.y = Input.GetAxis("Vertical1");

        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;

        // move the object
        transform.Translate(velocity * Time.deltaTime);
	}

}
