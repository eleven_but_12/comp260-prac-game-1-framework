﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour
{
    public Transform target1;
    public Transform target2;
    private Vector2 heading = Vector2.right;
    public float speed = 4.0f;        // metres per second
    public float turnSpeed = 180.0f;  // degrees per second
    public PlayerMove1 player;

    // Use this for initialization
    void Start ()
    {
        PlayerMove1 player =
              (PlayerMove1)FindObjectOfType(typeof(PlayerMove1));
        target1 = player.transform;
        PlayerMove2 player2 =
              (PlayerMove2)FindObjectOfType(typeof(PlayerMove2));
        target2 = player2.transform;

    }

    // Update is called once per frame
    void Update()
    {
        // get the vector from the bee to the target
        Vector2 one = target1.position - transform.position;
        Vector2 two = target2.position - transform.position;
        Vector2 direction;

        if (two.magnitude < one.magnitude)
        {
            direction = two;
        }
        else
        {
            direction = one;
        }

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);
    }

    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target1.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }


}
