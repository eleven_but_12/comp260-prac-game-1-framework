﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

    public int nBees = 50;
    public BeeMove beePrefab;
    public Rect spawnRect;

	// Use this for initialization
	void Start()
    {
        //create bees
        for (int i = 0; i < nBees; i++)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);
            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;

            // move the bee to a random position within 
            // the spawn rectangle
            float x = spawnRect.xMin +
                Random.value * spawnRect.width;
            float y = spawnRect.yMin +
                Random.value * spawnRect.height;

            bee.transform.position = new Vector2(x, y);
        }

    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

}
