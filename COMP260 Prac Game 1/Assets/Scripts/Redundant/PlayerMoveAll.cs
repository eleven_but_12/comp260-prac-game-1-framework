﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveAll : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public Vector2 move;
	public Vector2 velocity; // in metres per second
	public float maxSpeed = 5.0f;
    public string AxisX;
    public string AxisY;

	void Update() {
		
   // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis(AxisX);
        direction.y = Input.GetAxis(AxisY);

        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;

        // move the object
        transform.Translate(velocity * Time.deltaTime);
	}

}
